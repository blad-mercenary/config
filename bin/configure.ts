#! /bin/node

import { readFileSync, writeFileSync } from 'fs';
import * as path from 'path';

const packageJsonPath = path.resolve(process.cwd(), 'package.json');

type Data = Array<any> | Record<string, any>;

function add(base: Record<string, any>, key: string, data: Data) {
	if (!base[key]) {
		base[key] = data;
		return;
	}

	if (Array.isArray(base[key]) && Array.isArray(data)) {
		base[key] = Array.from(new Set(base[key].concat(data)));
		return;
	}

	if (typeof base[key] === 'object' && typeof data === 'object') {
		base[key] = {
			...base[key],
			...data
		};
		return;
	}

	throw new Error(`Cannot add property ${key}`);
}

function copy(source: string, dest = source) {
	const sourcePath = path.resolve(__dirname, '../..', source);
	const destPath = path.resolve(process.cwd(), dest);
	// console.log(`${source} => ${dest}`);
	const content = readFileSync(path.resolve(sourcePath));
	writeFileSync(destPath, content);
}

function setupPackageJson() {
	const packageJson = JSON.parse(readFileSync(packageJsonPath, { encoding: 'utf-8' }));

	const scripts = {
		docs: 'npx typedoc index.ts --out docs',
		ci: 'npm run lint && npm run recompile && npm run test-coverage && npm audit',
		prepublishOnly: 'npm run compile',
		'publish-local': 'npmrc local && npm publish',
		'publish-npm': 'npmrc default && npm publish',
		compile: 'npx tsc -b --verbose',
		recompile: 'rm -rf ./dist && npm run compile',
		lint: 'eslint \'./**/*.ts\'',
		'lint-fix': 'eslint \'./**/*.ts\' --fix',
		test: 'mocha',
		'test-ci': 'nyc --reporter text mocha',
		'test-coverage': 'nyc mocha'
	};

	add(packageJson, 'scripts', scripts);

	const files = [
		'/dist',
		'LICENSE',
		'package-lock.json',
		'package.json',
		'README.md'
	];

	add(packageJson, 'files', files);

	packageJson.license = 'MOZILLA PUBLIC LICENSE, VERSION 2.0';
	packageJson.nyc = undefined;
	packageJson.mocha = undefined;

	writeFileSync(packageJsonPath, JSON.stringify(packageJson, undefined, '\t'));
}

function setupConfigFiles() {
	const files = [
		'.eslintignore',
		'.eslintrc.json',
		'.gitlab-ci.yml',
		'.mocharc.json',
		'.nycrc.json',
		'jsdocrc.json',
		'LICENSE'
	];

	files.forEach((name) => {
		copy(name);
	});

	copy('_gitignore', '.gitignore');
	copy('_tsconfig.json', 'tsconfig.json');
}

setupPackageJson();
setupConfigFiles();
