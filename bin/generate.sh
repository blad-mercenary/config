#! /bin/sh

# Create npm project
npm init

# Common deps
npm i git+https://gitlab.com/blad-mercenary/utils.git#dev

# Common dev deps
npm i git+https://gitlab.com/blad-mercenary/config.git#dev git+https://gitlab.com/blad-mercenary/eslint-config.git#dev @istanbuljs/nyc-config-typescript @types/chai @types/inquirer @types/mocha @types/node @types/sinon @types/sinon-chai @typescript-eslint/eslint-plugin @typescript-eslint/parser chai eslint mocha nyc sinon sinon-chai source-map-support ts-node typedoc typescript
