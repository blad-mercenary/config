#! /bin/node
"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = require("fs");
var path = __importStar(require("path"));
var packageJsonPath = path.resolve(process.cwd(), 'package.json');
function add(base, key, data) {
    if (!base[key]) {
        base[key] = data;
        return;
    }
    if (Array.isArray(base[key]) && Array.isArray(data)) {
        base[key] = Array.from(new Set(base[key].concat(data)));
        return;
    }
    if (typeof base[key] === 'object' && typeof data === 'object') {
        base[key] = __assign(__assign({}, base[key]), data);
        return;
    }
    throw new Error("Cannot add property ".concat(key));
}
function copy(source, dest) {
    if (dest === void 0) { dest = source; }
    var sourcePath = path.resolve(__dirname, '../..', source);
    var destPath = path.resolve(process.cwd(), dest);
    var content = (0, fs_1.readFileSync)(path.resolve(sourcePath));
    (0, fs_1.writeFileSync)(destPath, content);
}
function setupPackageJson() {
    var packageJson = JSON.parse((0, fs_1.readFileSync)(packageJsonPath, { encoding: 'utf-8' }));
    var scripts = {
        docs: 'npx typedoc index.ts --out docs',
        ci: 'npm run lint && npm run recompile && npm run test-coverage && npm audit',
        prepublishOnly: 'npm run compile',
        'publish-local': 'npmrc local && npm publish',
        'publish-npm': 'npmrc default && npm publish',
        compile: 'npx tsc -b --verbose',
        recompile: 'rm -rf ./dist && npm run compile',
        lint: 'eslint \'./**/*.ts\'',
        'lint-fix': 'eslint \'./**/*.ts\' --fix',
        test: 'mocha',
        'test-ci': 'nyc --reporter text mocha',
        'test-coverage': 'nyc mocha'
    };
    add(packageJson, 'scripts', scripts);
    var files = [
        '/dist',
        'LICENSE',
        'package-lock.json',
        'package.json',
        'README.md'
    ];
    add(packageJson, 'files', files);
    packageJson.license = 'MOZILLA PUBLIC LICENSE, VERSION 2.0';
    packageJson.nyc = undefined;
    packageJson.mocha = undefined;
    (0, fs_1.writeFileSync)(packageJsonPath, JSON.stringify(packageJson, undefined, '\t'));
}
function setupConfigFiles() {
    var files = [
        '.eslintignore',
        '.eslintrc.json',
        '.gitlab-ci.yml',
        '.mocharc.json',
        '.nycrc.json',
        'jsdocrc.json',
        'LICENSE'
    ];
    files.forEach(function (name) {
        copy(name);
    });
    copy('_gitignore', '.gitignore');
    copy('_tsconfig.json', 'tsconfig.json');
}
setupPackageJson();
setupConfigFiles();
